﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BankDb_AbuRashid;

namespace BankDbTests_Abu_Rashid
{
    [TestClass]
    public class MathsTests_AbuRashid
    {
        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }
       

        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "C:\\Users\\Пользователь\\source\\repos\\Bank_Abu-Rashid\\BankDbTests_Abu-Rashid\\DbAbuRashid.csv", "DbAbuRashid#csv", DataAccessMethod.Sequential), DeploymentItem("DbAbuRashid.csv"), TestMethod]


        public void AddIntegers_FromDataSourceTest()
        {
            var target = new Maths_AbuRashid();

            // Access the data
            int x = Convert.ToInt32(TestContext.DataRow["FirstNumber"]);
            int y = Convert.ToInt32(TestContext.DataRow["SecondNumber"]);
            int expected = Convert.ToInt32(TestContext.DataRow["Sum"]);
            int actual = target.AddIntegers(x, y);
            Assert.AreEqual(expected, actual,
                "x:<{0}> y:<{1}>",
                new object[] { x, y });
        }
    }
}
